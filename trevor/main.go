package main

import (
	"fmt"
	"myapp/features"
)

func main() {
	fmt.Println("hello from main")
	//features.VariablesPractise()
	//features.StringInterpolation()
	//features.Pointers()
	//features.Slices()
	//features.Mapper()
	//features.Functions()
	//features.ReadConsoleInput()
	//fmt.Println(features.ExportedVariable)
	features.Functions()
}
