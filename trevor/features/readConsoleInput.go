package features

import (
	"bufio"
	"fmt"
	"os"

	"github.com/eiannone/keyboard"
)

func ReadConsoleInput() {

	keyboard.Open()

	reader := bufio.NewReader(os.Stdin)

	userInput, _ := reader.ReadString('\n')

	fmt.Println(userInput)
}
