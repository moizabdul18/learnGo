package features

import (
	"bufio"
	"fmt"
	"github.com/eiannone/keyboard"
	"os"
)

type User struct {
	username string
	dog      bool
}

var reader *bufio.Reader

func StringInterpolation() {
	fmt.Println()
	fmt.Println("hello from String interpolation")
	reader = bufio.NewReader(os.Stdin)

	var user User
	user.username = readString("what is your name?")
	user.dog = readBoolean("do you have a dog? (y/n)?")

	fmt.Printf("your name is: %s  and you own a dog? %t", user.username, user.dog)
}

func readString(question string) string {

	fmt.Println(question)
	prompt()

	name, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("error in name")
	}
	return name
}

func readBoolean(question string) bool {

	fmt.Println(question)
	prompt()

	char, _, err := keyboard.GetSingleKey()

	if err != nil {
		fmt.Println("error")
	}
	if char == 'N' || char == 'n' {
		return false
	}
	return true
}

func prompt() {
	fmt.Println("->")
}
