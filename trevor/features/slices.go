package features

import (
	"fmt"
	"sort"
)

func Slices() {
	fmt.Println()
	fmt.Println("hello from slices")
	var animals []string

	animals = append(animals, "dog")
	animals = append(animals, "cat")
	animals = append(animals, "cow")
	animals = append(animals, "goat")

	for _, x := range animals {
		fmt.Println(x)
	}

	for counter, x := range animals {
		fmt.Println(counter, x)
	}

	fmt.Println("index 2 goes to: ", animals[2])

	fmt.Println(animals[1:])

	fmt.Println("length is", len(animals))

	fmt.Println("sorted animals")
	sort.Strings(animals)
	for _, x := range animals {
		fmt.Println(x)
	}

}
