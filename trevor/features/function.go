package features

import "fmt"

type Animal struct {
	Name  string
	sound string
	legs  int
}

func Functions() {
	fmt.Println()
	fmt.Println("hello from function")
	var dog Animal

	dog.Name = "doggy"
	dog.sound = "booo boo"
	dog.legs = 4

	dog.Says()

	cat := Animal{
		Name:  "teddy",
		sound: "meow",
		legs:  2,
	}

	cat.Says()
}

func (animal *Animal) Says() {
	//  * because it is a struct, but works even without it
	fmt.Println(animal.Name, animal.sound, animal.legs)
}

// addTwoNumbers returns the sum of 2 ints
func addTwoNumbers(x, y int) int {
	return x + y
}

// naked return - rarely used, but possible, and only
// intended for short functions.
func add(x, y int) (sum int) {
	sum = x + y
	return
}

// variadic function - takes variable number of parameters
func sumMany(nums ...int) int {
	total := 0
	for _, x := range nums {
		total = total + x
	}

	return total
}
