package features

import "fmt"

func Pointers() {
	fmt.Println()
	fmt.Println("hello from Pointers")
	x := 10
	myFirstPointer := &x

	fmt.Printf("x : %d , pointer to x : %s", x, myFirstPointer)

	*myFirstPointer = 14
	fmt.Printf("changed value in x : %d", x)
	fmt.Println()
}
