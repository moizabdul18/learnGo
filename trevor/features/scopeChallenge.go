package features

import "fmt"

var PackageVar = "this is Package level variable in package one"

func PrintMe(inp string) {
	fmt.Print(inp)
}

func PrintMeAgain(s1 string, s2 string) {
	fmt.Println(s1, s2, PackageVar)
}

func PrintMeAgainAndAgain(s1, s2 string) {
	fmt.Println(s1, s2, PackageVar)
}
