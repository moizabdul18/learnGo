package features

import "fmt"

func Mapper() {
	fmt.Println()
	fmt.Println("hello from mapper")

	//var nilMap map[string]string // this will create empty(nil) map
	//nilMap["a"] = "b"

	firstMap := make(map[string]string)
	firstMap["a"] = "1"
	firstMap["b"] = "2"
	firstMap["c"] = "3"
	firstMap["d"] = "4"

	fmt.Println(firstMap)

	for key, value := range firstMap {
		fmt.Println(key, value) // no ordering
	}

	var qa = make(map[int]string)

	qa[1] = "a"

	fmt.Println(firstMap["b"])

	delete(firstMap, "c") // deleted c

	s, ok := firstMap["q"]
	if ok {
		fmt.Println("index of q", s)
	} else {
		fmt.Println("not exist")
	}

	for key, value := range firstMap {
		fmt.Println(key, value) // deleted c
	}
}
