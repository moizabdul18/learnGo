package features

import "fmt"

var ExportedVariable = "this is exported"

func VariablesPractise() {
	fmt.Println("hello from variables")

	var integ int
	var integer32 int32
	var floatt float32

	floatt = 100
	integer32 = 32
	integ = 12

	fmt.Printf("Integer %d %d  %f", integ, integer32, floatt)
	fmt.Println(ExportedVariable)
}
