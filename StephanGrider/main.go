package main

import (
	"fmt"
)

// run go run main.go deck.go
func main() {

	// variables
	new_var := "this is new variable"
	new_var = "variable is updated"
	fmt.Println(new_var)

	// functions
	new_var_from_Func := newVariable()
	fmt.Println(new_var_from_Func)

	// for loop
	listOfValues := deck{"abc", "def", "g"}

	cardSuite := deck{"spades", "diamonts", "hearts", "cc"}
	cardValues := deck{"ace", "two", "three", "four"}

	for _, suit := range cardSuite {
		for _, val := range cardValues {
			listOfValues = append(listOfValues, suit+" "+val)
		}
	}

	//	listOfValues.print()

	// multiple return values
	left, _ := deal(listOfValues, 4)

	left.print()
	//right.print()

	// map

	colors := map[string]string{
		"red":  "1",
		"blue": "2",
	}

	fmt.Println(colors)

	//var mapColors map[string]string

	mapColors := make(map[string]string)
	// always square bracjet with map
	mapColors["black"] = "3"
	mapColors["white"] = "4"
	mapColors["ping"] = "5"

	delete(mapColors, "white")
	fmt.Println(mapColors)
	fmt.Println("-----------------------")
	printMapParam(mapColors)

}

func printMapParam(m map[string]string) {

	for key, value := range m {
		fmt.Println(key, " ", value)
	}
}

// cant use map in receiver
// func (m map[string]string) printMap() {

// 	for key, value := range m {
// 		fmt.Println(key + " " + value)
// 	}
// }

func newVariable() string {
	return "this is from function"
}
