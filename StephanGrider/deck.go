package main

import "fmt"

type deck []string

func (d deck) print() {
	for index, value := range d {
		fmt.Println(index, value)
	}
}

func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]
}
