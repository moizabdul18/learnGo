package main

import (
	"bufio"
	"fmt"
	"mosh0616/learning/pkg/doctor"
	"os"
	"strings"
)

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Println(doctor.Intro())
	for {
		userInput, _ := reader.ReadString('\n')

		userInput = strings.Replace(userInput, "\r\n", "", -1)
		if userInput == "quit" {
			break
		} else {
			s := doctor.Response(userInput)
			fmt.Println(s)
		}
	}

}
